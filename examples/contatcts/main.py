"""
Uso simples:

>>>import contacts
>>> contacts.get()

contacts.get() return uma lista de dicionário que possui as chaves 'name' e 'number'

então contacts.get()[0]['name'] retorna o nome do primeiro contato da lista obtida e
contacts.get()[0]['number'] retorna o(os) número(s) do primeiro contato.

contacts.get()[1]['name'] retorna o nome do segundo contato da lista e
contacts.get()[1]['number'] retorna o(os) número(s) do segundo contato.

E assim sucessivamente.
"""



from kivy.app import App
from kivy.lang import Builder

from plyer import contacts

main_widget_kv = '''
Button:
    text: "Obter Contatos"
    on_release: app.get()
'''

class ContatctsApp(App):
    def build(self):
        main_widget = Builder.load_string(main_widget_kv)
        return main_widget

    def get(self):
        contatos = contacts.get()
        for c in contatos:
            print(c)


ContatctsApp().run()