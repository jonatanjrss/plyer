'''
Contacts
=====

The :class:`Contacts` is to read contacts list.

.. note::
        On Android the `READ_CONTACTS` permissions is needed.

Simple Examples
---------------

>>> from plyer import contacts
>>> print("I have {} contacts".format(len(contactos.get()))
I have 100 contacts
>>> print(contactos.get()[0])
{'name': <string>, 'number': <list>}


Supported Platforms
-------------------
Android

'''


class Contacts(object):
    '''
    Contacts Facade
    '''

    _contacts = []

    def __len__(self):
        return len(self._contacts)

    def __getitem__(self, item):
        return self._contacts[item]

    def __iter__(self):
        return self._contacts.__iter__()

    def get(self):
        """Returns all contacts
        :rtype: list
        """
        return self._contacts