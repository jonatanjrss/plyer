'''Implementation Contact for Android.'''

from jnius import autoclass
from plyer.facades import Contacts
from plyer.platforms.android import activity


ArrayList = autoclass('java.util.ArrayList')
String = autoclass('java/lang/String')
Phone = autoclass('android.provider.ContactsContract$CommonDataKinds$Phone')
contentResolver = activity.getContentResolver()
ContactsList = autoclass('android.provider.ContactsContract$Contacts')


class AndroidContacts(Contacts):
    '''Android Contacts
    .. versionadded:: 1.2.4
    '''

    def __init__(self):
        self.refresh()

    def refresh(self):
        """Refreshes local contact list"""
        cr = activity.getContentResolver()
        contact_cr = cr.query(ContactsList.CONTENT_URI, None, None, None, None)
        if contact_cr.getCount() < 1:
            return
        contacts = []
        
        
        while contact_cr.moveToNext():
            
            contact = {}
   
            name = contact_cr.getColumnIndex('display_name')
            account = contact_cr.getColumnIndex('account_name')
            type_ = contact_cr.getColumnIndex('account_type')
            
            contact['name'] = str(contact_cr.getString(name))
            contact['account'] = str(contact_cr.getString(account))
            contact['type_'] = str(contact_cr.getString(type_))

            phone_numbers = []
            contact_id = contact_cr.getColumnIndex("_id")
            l = ArrayList()
            l.add(contact_cr.getString(contact_id))
            query = String("contact_id=?")
            phone_uri = Phone.CONTENT_URI
            phone_cr = contentResolver.query(phone_uri, None, query, l.toArray(), None)

            while phone_cr.moveToNext():
                phone_number = phone_cr.getColumnIndex(Phone.NUMBER)
                phone_number = phone_cr.getString(phone_number)
                phone_numbers.append(phone_number)
                
            phone_cr.close()

            contact['number'] = phone_numbers
            contacts.append(contact)  
        contact_cr.close()

        self._contacts = contacts


def instance():
    return AndroidContacts()    